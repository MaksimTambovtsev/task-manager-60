package ru.tsc.tambovtsev.tm.repository;

public final class TaskRepositoryTest {
/*
    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    private static EntityManager entityManager;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user = new UserDTO();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @AfterClass
    public static void disconnectConnection() {
        entityManager.close();
        connectionService.close();
    }

    @Before
    public void setTaskRepository() {
        taskRepository = new TaskRepository(entityManager);
        userRepository = new UserRepository(entityManager);
        taskService = new TaskService(taskRepository, connectionService);
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        @Nullable TaskDTO task = new TaskDTO();
        user.setEmail("test@test.ru");
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userService.create(user);
        task.setUserId(user.getId());
        task.setName("123");
        task.setDescription("432");
        taskService.create(task);
        task = new TaskDTO();
        task.setUserId(user.getId());
        task.setName("1321");
        task.setDescription("234");
        taskService.create(task);
    }

    @After
    public void clearTaskRepository() {
        taskService.clearTask();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(user.getId(), Sort.NAME);
        @Nullable final String taskId = tasks.stream().findFirst().get().getId();
        Assert.assertFalse(taskRepository.findById(taskId).getName().isEmpty());
    }

    @Test
    public void testUpdateById() {
        @NotNull final TaskDTO task = taskRepository.findAll(user.getId(), Sort.NAME).get(1);
        task.setName("333-333-333-333");
        entityManager.getTransaction().begin();
        taskRepository.updateById(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskRepository.findById(task.getId()).getName(), "333-333-333-333");
    }

    @Test
    public void testRemoveById() {
        @NotNull final String taskId = taskRepository.findAll(user.getId(), Sort.NAME).get(1).getId();
        entityManager.getTransaction().begin();
        taskRepository.removeById(user.getId(), taskId);
        entityManager.getTransaction().commit();

        Assert.assertNull(taskRepository.findById(user.getId(), taskId));
    }

    @Test
    public void testClear() {
        entityManager.getTransaction().begin();
        taskRepository.clearTask();
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }
*/
}
