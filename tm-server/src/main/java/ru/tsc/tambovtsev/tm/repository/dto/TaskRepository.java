package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

@Repository
public class TaskRepository extends AbstractUserOwnedRepository<TaskDTO> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TaskDTO";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND PROJECT_ID = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @NotNull
    @Override
    protected Class<TaskDTO> getClassName() {
        return TaskDTO.class;
    }

}
