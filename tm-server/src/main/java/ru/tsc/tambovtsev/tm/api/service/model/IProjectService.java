package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

   @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
