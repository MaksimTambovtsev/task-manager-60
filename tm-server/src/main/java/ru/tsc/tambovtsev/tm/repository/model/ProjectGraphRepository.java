package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.List;

@Repository
public class ProjectGraphRepository extends AbstractUserOwnGraphRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "Project";

    @NotNull
    @Override
    protected Class<Project> getClassName() {
        return Project.class;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public void removeByIdProject(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME + " WHERE USER_ID = :userId AND ID = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
