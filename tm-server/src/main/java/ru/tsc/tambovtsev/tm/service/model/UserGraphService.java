package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IUserService;
import ru.tsc.tambovtsev.tm.api.service.property.ISessionPropertyService;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserGraphService extends AbstractGraphService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private ISessionPropertyService propertyService;

    @Nullable
    @Autowired
    private IUserRepository repository;

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = repository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @Nullable final User user = repository.findByEmail(email);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        repository.removeCascade(user);
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @Nullable final User user = repository.findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        repository.update(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        repository.lockUserById(user.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        repository.unlockUserById(user.getId());
    }

    @NotNull
    @Override
    public IUserRepository getRepository() {
        return repository;
    }

}
