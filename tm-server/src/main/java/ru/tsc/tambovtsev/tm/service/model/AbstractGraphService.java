package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IService;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractGraphService<M extends AbstractEntity, R extends IRepository<M>> implements IService<M> {

    @NotNull
    public abstract IRepository<M> getRepository();

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new UserNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        repository.addAll(models);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(id);
        if (result == null) return;
        repository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@NotNull final M model) {
        Optional.ofNullable(model).orElseThrow(NullPointerException::new);
        @NotNull final IRepository<M> repository = getRepository();
        repository.create(model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.update(model);
    }

    @Override
    @Transactional
    public void removeCascade(@NotNull M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.removeCascade(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
        repository.addAll(models);
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
    }

}
