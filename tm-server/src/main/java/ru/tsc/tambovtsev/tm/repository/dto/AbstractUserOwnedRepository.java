package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import java.util.List;

@Repository
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends AbstractRepository<M>
        implements IOwnerRepository<M> {

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @Nullable M findById(@Nullable String userId, @Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id AND USER_ID = :userId", getClassName())
                .setParameter("id", id)
                .setParameter("userId", userId));
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM " + getTableName() + " e WHERE USER_ID = :userId", Long.class)
                .setParameter("userId", userId)
                .getResultList()
                .get(0);
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND ID = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @Nullable List<M> findAll(@Nullable String userId, @Nullable Sort sort) {
        return entityManager
                .createQuery("FROM " + getTableName() + " ORDER BY " + sort.name() + " ASC", getClassName())
                .getResultList();
    }

}
