package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IOwnerRepository<M>, IService<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
