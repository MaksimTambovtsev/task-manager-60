package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    @Nullable
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
