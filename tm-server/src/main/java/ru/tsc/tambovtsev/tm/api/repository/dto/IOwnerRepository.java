package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import java.util.List;

public interface IOwnerRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

    void clear(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
