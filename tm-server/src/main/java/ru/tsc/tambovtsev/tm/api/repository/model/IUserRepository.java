package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void lockUserById(@NotNull String id);

    void unlockUserById(@NotNull String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    void removeByLogin(@Nullable String login);

}
