package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

@Repository
public class ProjectRepository extends AbstractUserOwnedRepository<ProjectDTO> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "ProjectDTO";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    protected Class<ProjectDTO> getClassName() {
        return ProjectDTO.class;
    }

}
