package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractEntityDTO> extends IRepository<M> {

    @Nullable
    M findById(@Nullable String id);

    void removeById(@Nullable String id);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void create(@Nullable M model);

    void update(@NotNull M model);

    @Nullable
    List<M> findAll();

    void clear();

}

