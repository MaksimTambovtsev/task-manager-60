package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Optional;

@Service
public class ProjectTaskGraphService implements IProjectTaskService {

    @Nullable
    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Autowired
    private ITaskRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @Nullable final Task task = repository.findById(userId, taskId);
        @Nullable final Project project = projectRepository.findById(userId, projectId);
        if (task == null) return;
        task.setProject(project);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @Nullable final Task task = repository.findById(userId, taskId);
        if (task == null) return;
        task.setProject(null);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @Nullable final Project project = projectRepository.findById(userId, projectId);
        if (project == null) return;
        projectRepository.removeCascade(project);
    }

}
