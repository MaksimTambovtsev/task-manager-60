package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.List;

@Repository
public class TaskGraphRepository extends AbstractUserOwnGraphRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "Task";

    @NotNull
    @Override
    protected Class<Task> getClassName() {
        return Task.class;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND PROJECT_ID = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
