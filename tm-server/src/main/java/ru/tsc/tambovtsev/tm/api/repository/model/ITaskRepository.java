package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);

}
