package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<TaskDTO> {

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);

}
