package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    void lockUserById(@NotNull String id);

    void unlockUserById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void removeByLogin(@Nullable String login);

}
