package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void removeByIdProject(@Nullable String userId, @Nullable String id);

}