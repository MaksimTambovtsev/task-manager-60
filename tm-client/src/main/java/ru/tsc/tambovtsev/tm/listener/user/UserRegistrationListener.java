package ru.tsc.tambovtsev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.UserRegistryRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class UserRegistrationListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-registry-profile";

    @NotNull
    public static final String DESCRIPTION = "Registration user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRegistrationListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[USER REGISTRATION PROFILE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        getUserEndpoint().registryUser(new UserRegistryRequest(login, password, email));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
