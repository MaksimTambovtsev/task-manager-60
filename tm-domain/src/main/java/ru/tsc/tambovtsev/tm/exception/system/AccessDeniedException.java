package ru.tsc.tambovtsev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException(@NotNull Throwable cause) {
        super(cause);
    }

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
